import kotlinx.coroutines.*
import kotlin.system.*

fun main() = runBlocking<Unit> {
    val time1 = measureTimeMillis {
        val one = first()
        val two = second()
        println("Ответ = ${one + two}")
    }
    println("Время компиляции: $time1 ms")

    val time2 = measureTimeMillis {
        val one = async { first() }
        val two = async { second() }
        println("Ответ = ${one.await() + two.await()}")
    }
    println("Время компиляции: $time2 ms")

    println(
        "Очевидно, что $time2 < $time1, так как\nво втором случае используется\n" +
                "параллельный(асинхронный) вызов функций,\nчто значит, что функции вызываются\n" +
                "в одно и то же время, следовательно,\nзадержка в одну секунду начинается так же " +
                "в одно и то же время"
    )
}
suspend fun first(): Int {
    delay(1000L)
    return 13
}

suspend fun second(): Int {
    delay(1000L)
    return 29
}